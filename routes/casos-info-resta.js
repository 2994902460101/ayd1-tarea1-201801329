var express = require('express');
var router = express.Router();

router.post('/',  function(req, res) {
    const data_suma = {
        numero1 : req.body.numero1,
        numero2 : req.body.numero2        
    }
    res.status(200).json(data_suma.numero1 - data_suma.numero2)
});



module.exports = router;