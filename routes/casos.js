var express = require('express');
var router = express.Router();

router.post('/',  function(req, res) {
    const data_suma = {
        numero1 : req.body.numero1,
        numero2 : req.body.numero2        
    }
    res.status(200).json(data_suma.numero1 + data_suma.numero2)
});

router.get('/',  function(req, res) {

    console.log('Servidor en el puerto 3000');
    res.status(200).json('Servidor en el puerto 3000')
    
});

module.exports = router;